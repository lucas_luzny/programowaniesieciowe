package Laboratorium1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

class Base64Decoder {
    private final static String BASE64CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    static byte[] decode(String file) throws IOException {
        ByteArrayOutputStream decodedBytes = new ByteArrayOutputStream();
        int padding = 0;

        /*Loop over all base64 encoded chars in group of 4*/
        for (int i = 0; i < file.length(); i += 4) {

            /*Retrieve 6 bits of first base 64 encoded char in the group and place it in positions 19-24 of group of bits*/
            int groupOfBits = ((BASE64CHARS.indexOf(file.charAt(i)) & 0xFF) << 18) & 0xFFFFFF;

            /*Retrieve 6 bits of second base 64 encoded char in the group and place it in positions 13-18 of group of bits*/
            groupOfBits |= (BASE64CHARS.indexOf(file.charAt(i + 1)) & 0xFF) << 12;

            /*Retrieve 6 bits of third base 64 encoded char in the group and place it in positions 7-12 of group of bits (if there is no padding)*/
            if (file.charAt(i + 2) != '=') {
                groupOfBits |= (BASE64CHARS.indexOf(file.charAt(i + 2)) & 0xFF) << 6;
            } else {
                padding ++;
            }

            /*Retrieve 6 bits of last base 64 encoded char in the group and place it in positions 1-6 of group of bits (if there is no padding)*/
            if (file.charAt(i + 3) != '=') {
                groupOfBits |= (BASE64CHARS.indexOf(file.charAt(i + 3)) & 0xFF);

            } else {
                padding ++;
            }

            byte[] threeDecodedBytes = new byte[3 - padding];

            /*Retrieve last 8 bits of 24 bit group*/
            threeDecodedBytes[0] = (byte) ((groupOfBits >> 16) & 0xFF);

            /*Retrieve next 8 bits of 24 bit group*/
            if (!(padding > 0)) {
                threeDecodedBytes[1] = (byte) ((groupOfBits >> 8) & 0xFF);
            }

            /*Retrieve last 8 bits of 24 bit group*/
            if (!(padding > 1)){
                threeDecodedBytes[2] = (byte) (groupOfBits & 0xFF);
            }

            decodedBytes.write(threeDecodedBytes);
        }

        return decodedBytes.toByteArray();
    }
}