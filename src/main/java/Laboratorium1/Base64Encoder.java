package Laboratorium1;

class Base64Encoder {
    private final static String BASE64CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    static String encode(byte[] file) {
        StringBuilder encodedString = new StringBuilder();
        int padding = 0;

        /*Loop over all bits in file in a group of 3 bits*/
        for (int i = 0; i < file.length; i += 3) {

            /*Use 0xFF to mask (make 0) all but the lowest 8 bits of the number, then shift those 8 bits 16 positions to left (8 highest bits of the group)in base64 24-bit group of bits.
             The same is done with 0xFFFFFF to ensure that all but 24 lowest 8 bits are zeroes */
            int groupOfBits = ((file[i] & 0xFF) << 16) & 0xFFFFFF;

            /*Use bitwise or operator to fill bits in positions 9-16*/
            if (i + 1 < file.length) {
                groupOfBits |= (file[i+1] & 0xFF) << 8;
            } else {
                padding++;
            }

            /*Use bitwise or operator to fill bits in positions 1-8*/
            if (i + 2 < file.length) {
                groupOfBits |= (file[i+2] & 0xFF);
            } else {
                padding++;
            }

            for (int j = 0; j < 4 - padding; j++) {
                /*Use & 0xFC0000 to ensure that all but only last 6 highest bits are zeroes and then shift those 6 bits 18 positions to the left to get base64 char index*/
                int base64CharIndex = (groupOfBits & 0xFC0000) >> 18;
                encodedString.append(BASE64CHARS.charAt(base64CharIndex));
                groupOfBits <<= 6;
            }
        }

        /* Add padding if necessary*/
        if (padding > 0) {
            for (int j = 0; j < padding; j++) {
                encodedString.append("=");
            }
        }

        return encodedString.toString();
    }
}
