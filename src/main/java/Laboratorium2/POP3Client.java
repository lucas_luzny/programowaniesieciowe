package Laboratorium2;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.*;

public class POP3Client {
    private Properties props;
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private int totalNumberOfMessages;
    private ArrayList messagesUIDLs = new ArrayList();
    private boolean initialUIDLCheck = true;
    private int newMessagesCount = 0;
    private Scanner scan;

    private void loadProps() throws IOException {
        props = new Properties();
        InputStream is = POP3Client.class.getClassLoader().getResourceAsStream("lab2.properties");
        props.load(is);
        is.close();
    }

    private void connect (String host, int port) throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(host, port));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        reader.readLine();
    }

    private String sendPOP3Command(String command) throws IOException {
        writer.write(command + "\n");
        writer.flush();
        return reader.readLine();
    }

    private void authenticate(String user, String password) throws IOException {
        sendPOP3Command("USER " + user);
        sendPOP3Command("PASS " + password);
    }

    private int getNumberOfMessages() throws IOException {
        String response = sendPOP3Command("STAT");
        String[] parsedResponse = response.split(" ");
        return Integer.parseInt(parsedResponse[1]);
    }

    private void getUIDLsForMessages(int numberOfMessages) throws IOException {
        for (int i = 1; i <= numberOfMessages; i++) {
            String response = sendPOP3Command("UIDL " + i);

            if (response.indexOf("ERR") < 0) {
                String[] parsedResponse = response.split(" ");
                if (!messagesUIDLs.contains(parsedResponse[2])) {
                    messagesUIDLs.add(parsedResponse[2]);
                    if (!initialUIDLCheck) {
                        newMessagesCount++;
                        System.out.println("There's a new message in inbox with UIDL: " + parsedResponse[2]);
                    }
                }
            }
        }

        initialUIDLCheck = false;
    }

    private void logout() throws IOException {
        sendPOP3Command("QUIT");
    }

    private void disconnect() throws IOException {
        socket.close();
    }

    public static void main(String[] args) throws IOException {
        POP3Client pop3Client = new POP3Client();
        pop3Client.loadProps();
        System.out.println(pop3Client.props);

        pop3Client.connect(pop3Client.props.getProperty("host"), Integer.parseInt(pop3Client.props.getProperty("port")));
        pop3Client.authenticate(pop3Client.props.getProperty("user"), pop3Client.props.getProperty("password"));

        pop3Client.totalNumberOfMessages = pop3Client.getNumberOfMessages();
        System.out.println("Total number of emails: " + pop3Client.totalNumberOfMessages);
        pop3Client.getUIDLsForMessages(pop3Client.totalNumberOfMessages);
        pop3Client.logout();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    System.out.println("Sending next request to POP3 Server");
                    pop3Client.connect(pop3Client.props.getProperty("host"), Integer.parseInt(pop3Client.props.getProperty("port")));
                    pop3Client.authenticate(pop3Client.props.getProperty("user"), pop3Client.props.getProperty("password"));
                    pop3Client.totalNumberOfMessages = pop3Client.getNumberOfMessages();
                    pop3Client.getUIDLsForMessages(pop3Client.totalNumberOfMessages);
                    pop3Client.logout();

                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        }, 0, 1000 * Integer.parseInt(pop3Client.props.getProperty("time")));

        pop3Client.scan = new Scanner(System.in);
        
        while (pop3Client.scan.hasNext()) {
            String userInput  = pop3Client.scan.next();
            if (userInput.equalsIgnoreCase("Q")) {
                timer.cancel();
                pop3Client.logout();
                pop3Client.disconnect();
                System.out.println("Thank you for using this app. From the beginning of your's journey there have been: " + pop3Client.newMessagesCount + " new messages.");
                System.exit(0);
            }
        }
    }
}
