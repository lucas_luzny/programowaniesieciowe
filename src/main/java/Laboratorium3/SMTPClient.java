package Laboratorium3;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Properties;

public class SMTPClient {
    private Properties props;
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;

    private void loadProps() throws IOException {
        props = new Properties();
        InputStream is = SMTPClient.class.getClassLoader().getResourceAsStream("lab3.properties");
        props.load(is);
        is.close();
    }

    private void sendMail() throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(props.getProperty("smtp"), Integer.parseInt(props.getProperty("port"))));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        String from = "<" + props.getProperty("user") + ">";
        String to = "<" + props.getProperty("to") + ">";
        String user = props.getProperty("user");
        String pass = props.getProperty("password");
        String username = Base64.getEncoder().encodeToString(user.getBytes(StandardCharsets.UTF_8));
        String password = Base64.getEncoder().encodeToString(pass.getBytes(StandardCharsets.UTF_8));

        sendSMTPCommand("EHLO", true);
        sendSMTPCommand("AUTH LOGIN ", true);
        sendSMTPCommand(username, true);
        sendSMTPCommand(password, true);
        sendSMTPCommand("MAIL FROM:" + from, true);
        sendSMTPCommand("RCPT TO:" + to, true);
        sendSMTPCommand("DATA", true);
        sendSMTPCommand("From: " + from, false);
        sendSMTPCommand("To: " + to, false);
        sendSMTPCommand("Subject: PS LAB N2 ZIMA 2018 14A", false);
        sendSMTPCommand("Lukasz Luzny", false);
        sendSMTPCommand(".", true);
        sendSMTPCommand("QUIT", true);
    }

    private void sendSMTPCommand(String command, boolean readResponse) throws IOException {
        System.out.println("Client: " + command);
        writer.write(command + "\n");
        writer.flush();

        if (readResponse) {
            String line;
            do {
                line = reader.readLine();
                System.out.println("Server: " + line);
            }
            while (reader.ready());
        }
    }

    public static void main(String[] args) throws IOException {
        SMTPClient smtpClient = new SMTPClient();
        smtpClient.loadProps();
        smtpClient.sendMail();
    }
}
