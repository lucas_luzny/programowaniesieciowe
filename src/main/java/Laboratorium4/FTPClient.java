package Laboratorium4;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FTPClient {
    private Properties props;
    private Socket socket;
    private Socket passiveSocket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private BufferedReader passiveReader;
    private Scanner menuInput;

    private void loadProps() throws IOException {
        props = new Properties();
        InputStream is = FTPClient.class.getClassLoader().getResourceAsStream("lab4.properties");
        props.load(is);
        is.close();
    }

    private void connect(String host, int port) throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(host, port));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    private void authenticate(String user, String password) throws IOException, InterruptedException {
        sendFTPCommand("USER " + user);
        sendFTPCommand("PASS " + password);
    }

    private void sendFTPCommand(String command) throws IOException, InterruptedException {
        writer.write(command + "\n");
        writer.flush();
        String line;
        do {
            line = reader.readLine();
//            System.out.println("Server: " + line);
            Thread.sleep(100);
        }
        while (reader.ready());
    }

    private void menu() throws IOException, InterruptedException {
        String selection;
        menuInput = new Scanner(System.in);

        System.out.println();
        System.out.println("Choose one of the following options");
        System.out.println("-------------------------\n");
        System.out.println("1 - PWD - Print Working Directory");
        System.out.println("2 - CWD - Change Working Directory");
        System.out.println("3 - CDUP - Change to Parent Directory");
        System.out.println("4 - LIST - list information of current working directory");
        System.out.println("5 - LIST(dir) - list information of specified directory");
        System.out.println("6 - TREE - print server tree");
        System.out.println("7 - Quit");

        while (menuInput.hasNext()) {
            selection = menuInput.next();

            switch (selection) {
                case "1":
                    System.out.println("Option 1 have been selected");
                    sendFTPCommand("PWD");
                    menu();
                    break;
                case "2":
                    System.out.println("Option 2 have been selected");
                    System.out.println("Please provide name of catalog you want to move to:");
                    String dir = menuInput.next();
                    sendFTPCommand("CWD " + dir);
                    menu();
                    break;
                case "3":
                    System.out.println("Option 3 have been selected");
                    sendFTPCommand("CDUP");
                    menu();
                    break;
                case "4":
                    System.out.println("Option 4 have been selected");
                    openPassiveConnection();
                    sendFTPCommand("LIST");
                    printLISTResponse(readPassiveServerResponse());
                    menu();
                    break;
                case "5":
                    System.out.println("Option 5 have been selected");
                    openPassiveConnection();
                    System.out.println("Please provide name of directory (type 'cdir' for Current Directory):");
                    selection = menuInput.next();
                    if (selection.equalsIgnoreCase("cdir")) {
                        sendFTPCommand("LIST");
                    } else {
                        sendFTPCommand("LIST " + selection);
                    }
                    menu();
                    break;
                case "6":
                    System.out.println("Option 6 have been selected");
                    printServerTree("", 0);
                    menu();
                    break;
                case "7":
                    System.out.println("Option 7 have been selected");
                    disconnect();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Option you have selected is not available");
            }
        }
    }

    private void openPassiveConnection() throws IOException, InterruptedException {
        writer.write("PASV" + "\n");
        writer.flush();

        String line;
        do {
            line = reader.readLine();
//            System.out.println("Server: " + line);
            Thread.sleep(100);
        }
        while (reader.ready());

        Pattern pattern = Pattern.compile("([\\d]+)");
        Matcher matcher = pattern.matcher(line);
        ArrayList<String> resp = new ArrayList<>();
        matcher.matches();

        while (matcher.find()) {
            resp.add(matcher.group(1));
        }

        String host = resp.get(0).concat(".")
                .concat(resp.get(1)).concat(".")
                .concat(resp.get(2)).concat(".")
                .concat(resp.get(3));

        int port = Integer.parseInt(resp.get(4)) * 256 + Integer.parseInt(resp.get(5));
        this.passiveSocket = new Socket(host, port);
    }

    private ArrayList<String> readPassiveServerResponse() throws IOException {
        passiveReader = new BufferedReader(new InputStreamReader(passiveSocket.getInputStream()));
        ArrayList<String> lines = new ArrayList<>();
        String line;
        do {
            line = passiveReader.readLine();
            lines.add(line);
        }
        while (passiveReader.ready());
        return lines;
    }

    private void printLISTResponse(ArrayList<String> lines) {
        System.out.println();
        System.out.println("---------------------------------");
        if (lines.size() > 0) {
            lines.forEach(System.out::println);
        } else {
            System.out.println("No such directory or directory is empty.");

        }
        System.out.println("---------------------------------");
    }

    private void printServerTree(String dir, int tab) throws IOException, InterruptedException {
        openPassiveConnection();
        String tabSpace = "";
        for(int i = 0; i < tab; i++)
        {
            tabSpace += "\t";
        }
        tab++;
        sendFTPCommand("LIST " + dir + "\r\n"  );

        Thread.sleep(1000);

        ArrayList<String> resp = readPassiveServerResponse();
        for (String line : resp) {
            String[] splittedLine = line.split("\\s+");
            String fileOrDirName = splittedLine[splittedLine.length-1];
            if (!(fileOrDirName.equals(".") || fileOrDirName.equals(".."))) {
                System.out.println(tabSpace + fileOrDirName);
                if (splittedLine[0].startsWith("d")){
                    printServerTree(dir + fileOrDirName + "/", tab);
                }
            }
        }

    }

    private void disconnect() throws IOException {
        socket.close();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        FTPClient ftpClient = new FTPClient();
        ftpClient.loadProps();
        ftpClient.connect(ftpClient.props.getProperty("host"), Integer.parseInt(ftpClient.props.getProperty("port")));
        ftpClient.authenticate(ftpClient.props.getProperty("user"), ftpClient.props.getProperty("password"));
        ftpClient.menu();
    }
}
